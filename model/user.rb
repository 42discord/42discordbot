class User < ApplicationRecord

  self.table_name = 'users'
  self.primary_key = 'id'

  def recheck_stud
    make_stud
  end

  def recheck_roles
    make_roles
  end

  def recheck_campus
    make_campus
  end

  def make_all
    puts self.login
    raise StandardError unless (@ft_user = fetch_intra)

    return if @ft_user == :not_reachable || cursus_ended

    make_stud
    #make_roles
    make_campus
  rescue StandardError => e
     puts "Error with #{self.login} in make_all : #{e.message}"
  end

  private

  def fetch_intra
    retries ||= 0
    FTBot.instance.ft_client.get("/v2/users/#{self.login}").parsed
  rescue OAuth2::Error => e
    return :not_reachable if e.message == ": \n{}"

    sleep(TOKEN_REQUEST_SECOND)
    retry if (retries += 1) < 5
  end

  def cursus_ended
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    cursus = @ft_user['cursus_users'].select { |k, _| true if k["cursus_id"].to_i == 21 || k["cursus_id"].to_i == 1 }
    if cursus&.all? { |c| c['end_at'] && c['end_at'].to_time <= Time.now }
      member = ft_server.member(self.discordid)
      member&.remove_role(hash_arg[:server]['student_role']) if member&.role?(hash_arg[:server]['student_role'])
      alumni_role = ft_server.role(hash_arg[:server]['alumni_role'])
      member&.add_role(alumni_role)
      member&.roles&.each { |r| member&.remove_role(r.id) if r.position < alumni_role.position }
      self.destroy
      return true
    end
    false
  end

  def make_stud
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    return if ft_server&.member(self.discordid)&.role?(hash_arg[:server]['student_role']) && !@ft_user['cursus_users'].detect { |k, _| true if k["cursus_id"].to_i == 21 || k["cursus_id"].to_i == 1 }

    cursus = @ft_user['cursus_users'].select { |k, _| true if k["cursus_id"].to_i == 21 || k["cursus_id"].to_i == 1 }
    return unless cursus&.any? { |c| c['begin_at'].to_time <= Time.now }

    member = ft_server.member(self.discordid)
    member&.remove_role(hash_arg[:server]['cadet_role']) if member&.role?(hash_arg[:server]['cadet_role'])
    member&.remove_role(hash_arg[:server]['curious_role']) if member&.role?(hash_arg[:server]['curious_role'])
    member&.remove_role(hash_arg[:server]['alumni_role']) if member&.role?(hash_arg[:server]['alumni_role'])
    member&.add_role(hash_arg[:server]['student_role'])
  end

  def make_roles
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    projects = []
    arr_roles = {}
    return unless ft_server&.member(self.discordid)&.role?(hash_arg[:server]['student_role'])

    member = ft_server.member(self.discordid)
    @ft_user['projects_users'].each do |info|
      next if info["project"]["name"].capitalize_first.start_with?("Day", "Rush", "Hackathon", "Peer V", "Duration",
                                                                   "Contract Upload", "Entrepreneurship final",
                                                                   "Entrepreneurship mid", "Company",
                                                                   "Startup Internship - ", "Internship I - ",
                                                                   "Internship II - ", "Part_Time I - ", "Part_Time I ",
                                                                   "Startup Internship",
                                                                   "C Exam Alone In The Dark - ", "Old-CPP Module 01",
                                                                   "Old-CPP Module 01", "Old-CPP Module 02",
                                                                   "Old-CPP Module 03", "Old-CPP Module 04",
                                                                   "Old-CPP Module 05", "Old-CPP Module 06",
                                                                   "Old-CPP Module 07", "Old-CPP Module 00",
                                                                   "CPP Module 00", "CPP Module 01",
                                                                   "CPP Module 02", "CPP Module 03",
                                                                   "CPP Module 04", "CPP Module 05", "CPP Module 06",
                                                                   "CPP Module 07")

      projects << "#{info["project"]["name"]}⭐".capitalize_first if (info["cursus_ids"][0].to_i == 1 || info["cursus_ids"][0].to_i == 21) && info["status"].to_s == "finished" && info["validated?"] == true
    end

    ft_server.roles.each { |r| arr_roles[r.name] = r }

    projects.each do |p|
      # arr_roles[p] = ft_server.create_role(name: p, colour: rand(16777215), mentionable: false, permissions: Discordrb::Permissions.new([])) if arr_roles[p].nil?
      member.add_role(arr_roles[p]) unless member.role?(arr_roles[p]) unless arr_roles[p].nil?
    end

  end

  def make_campus
    good_hash = nil
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    member = ft_server.member(self.discordid)
    campus = @ft_user['campus_users'].sort_by! { |hsh| hsh['is_primary'] ? 0 : 1 }.first
    hash_campus = []
    hash_campus.push({ name: 'paris', id: hash_arg[:server]['campus_paris_id'], role: hash_arg[:server]['campus_paris_role'] })
    hash_campus.push({ name: 'lyon', id: hash_arg[:server]['campus_lyon_id'], role: hash_arg[:server]['campus_lyon_role'] })
    hash_campus.push({ name: 'nice', id: hash_arg[:server]['campus_nice_id'], role: hash_arg[:server]['campus_nice_role'] })
    hash_campus.push({ name: 'quebec', id: hash_arg[:server]['campus_quebec_id'], role: hash_arg[:server]['campus_quebec_role'] })
    hash_campus.push({ name: 'heilbronn', id: hash_arg[:server]['campus_heilbronn_id'], role: hash_arg[:server]['campus_heilbronn_role'] })
    hash_campus.each { |hash| good_hash = hash if hash[:id] == campus['campus_id'] }

    unless good_hash.nil?
      member.add_role(good_hash[:role]) unless member.role?(good_hash[:role])
    end

  end
end

User.columns.each { |column|
  puts column.name
  puts column.type
}

puts "User Model Initialized !".green