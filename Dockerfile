# ruby 2.5.1 & node 10.4
FROM starefossen/ruby-node:2-10

WORKDIR /app

#COPY Gemfile Gemfile.lock ./
COPY Gemfile ./

ENV BUNDLER_VERSION=2.1.4
RUN gem update --system && gem install bundler:2.1.4
RUN bundle install

COPY . .

CMD bundle exec ruby main.rb