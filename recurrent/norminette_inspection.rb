AWAIT_TIMEOUT = 300

@bot.message(contains: /```c\s.*?```/im) do |event|
  next if event.message.content.start_with?(/#{event.bot.prefix}norminette/)

  code = /(?<=```c\s).*?(?=```)/im.match(event.message.content).to_s
  code = tweak_code(code)
  code_file = Tempfile.new(%w[auto_norminette .c])
  code_file.write("\n#{code}")
  code_file.rewind
  event.message.react("omatic:699053014987571220")
  norm_react((norm_reply = norminette(['-R', 'CheckFilename', '-R', 'CheckTopCommentHeader', code_file.path])).lines.count == 1, event)
  code_file.close
  code_file.unlink
  unless norm_reply.lines.count == 1
    @bot.add_await!(Discordrb::Events::ReactionAddEvent, timeout: AWAIT_TIMEOUT) do |await_event|
      next false if event.user.id != await_event.user.id || !%w[omatic ❌].include?(await_event.emoji.name)

      event.channel.split_send norm_reply
      true
    end
  end
  nil
end

def norm_react(value, event)
  value ? event.message.react("✅") : event.message.react("❌")
end

puts 'NorminetteInspection recurrent Loaded !'.green