@bot.reaction_add do |event|
  collection = @mongo_db[:react_role]
  next unless collection.find(message_id: event.message.id).first

  unreact(event)
  next unless (data = collection.find(message_id: event.message.id).first[:list].find { |v| event.emoji.to_reaction == v[:emoji] })

  student_role = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['student_role']
  next if @bot.server(@bot.server_id).member(event.user.id).role?(student_role)

  if @bot.server(@bot.server_id).member(event.user.id).role?(data[:role_id])
    next unless data[:unset]

    @bot.server(@bot.server_id).member(event.user.id).remove_role(@bot.server(@bot.server_id).role(data[:role_id]))
  else
    @bot.server(@bot.server_id).member(event.user.id).add_role(@bot.server(@bot.server_id).role(data[:role_id]))
  end
end


puts 'ReactRole recurrent Loaded !'.green
