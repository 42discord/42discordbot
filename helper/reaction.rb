def unreact(event)
  event.message.delete_reaction(event.user, "#{event.emoji.name}:#{event.emoji.id}") unless event.emoji.id.nil?
  event.message.delete_reaction(event.user, event.emoji.name) unless event.emoji.id
end