def imgur_connect
  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), 'config/personal_config.yml'))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:imgur]
  client = Imgur.new(hash_arg['UID'])
  puts 'Imgur API Initialized !'.green if client
  client
rescue StandardError => e
  puts "Imgur error : #{e.backtrace} #{e}"
  puts 'Imgur failed to Initialize !'.red
end