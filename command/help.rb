@bot.command(:help, aliases: [:man], help_available: false) do |event, target|
  i = 0
  list_cmd = @bot.commands
  return targeted_help(target.to_sym, list_cmd, event) if target

  list_cmd = list_cmd.map do |cmd|
    cmd if cmd[1].class == Discordrb::Commands::Command && cmd[1].attributes[:help_available] == true
  end.compact
  list_slice_cmd = list_cmd.each_slice(15)
  list_slice_cmd.each do |slice_cmd|
    event.channel.send_embed do |embed|
      if (i += 1) == 1
        embed.description = '**Liste des commandes**'
        embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: event.server.name, icon_url: event.server.icon_url)
      end
      embed.colour = '#46eb34'
      slice_cmd.each do |cmd|
        embed.add_field(value: "#{
cmd[1].attributes[:description] || 'No description'}\n**usage**: #{
cmd[1].attributes[:usage] || 'No usage'}", name: "#{
cmd[1].name} #{"`#{cmd[1].attributes[:required_permissions]}`" unless cmd[1].attributes[:required_permissions].empty?}")
      end
      if i == list_slice_cmd.size
        embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
        embed.timestamp = Time.now
      end
    end
  end
end

def targeted_help(target, list_cmd, event)
  return "This command does not exist !" unless (cmd = list_cmd[target])

  cmd = cmd.aliased_command if cmd.class == Discordrb::Commands::CommandAlias
  event.channel.send_embed do |embed|
    embed.description = "**Aide de la commande #{cmd.name}**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: event.server.name, icon_url: event.server.icon_url)
    embed.colour = '#46eb34'
    embed.add_field(value: "#{
    cmd.attributes[:description] || 'No description'}\n**usage**: #{
    cmd.attributes[:usage] || 'No usage'}", name: "#{
    cmd.name} #{"`#{cmd.attributes[:required_permissions]}`" unless cmd.attributes[:required_permissions].empty?}")
    embed.add_field(value: "`#{cmd.attributes[:aliases]}`", name: "Aliases") unless cmd.attributes[:aliases].empty?
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
  end
end

puts 'Help CMD Loaded !'.green
