ADMIN_ARRAY = [472454954121035776, 140198028236685312, 165944063655149568, 138440712701214721, 456845128116469790].freeze
@eval_threads = {}

@bot.command(:eval, min_args: 1, required_permissions: [:ban_members], help_available: false, permission_message: "<:cestNon:779464483969957888>") do |event, *args|
  next unless ADMIN_ARRAY.include?(event.user.id)

  thread_uuid = SecureRandom.uuid
  @eval_threads[thread_uuid] = Thread.new { eval(args.join(' ')) }
  event.respond "Thread uuid = #{thread_uuid}"
  @eval_threads[thread_uuid].join
  @eval_threads.delete(thread_uuid)
  event.message.react('✅')
end

@bot.command(:kill, min_args: 1, required_permissions: [:ban_members], help_available: false, permission_message: "<:cestNon:779464483969957888>") do |event, uuid|
  next unless ADMIN_ARRAY.include?(event.user.id)

  if @eval_threads[uuid]
    Thread.kill(@eval_threads[uuid])
    @eval_threads.delete(uuid)
    event.respond "Thread #{uuid} has been killed"
  else
    event.respond "Thread #{uuid} does not exist"
  end
end

@bot.command(:display_roles, required_permissions: [:ban_members], help_available: false) do |event|
  event.server.roles.each { |x| puts "Name: #{x.name} - ID: #{x.id} - Position: #{x.position}" }
  event.message.react('✅')
end

@bot.command(:react_role, required_permissions: [:ban_members], help_available: false) do |event|
  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/react_role_config.yml")))
  event.message.delete
  @mongo_db[:react_role].drop
  collection = @mongo_db[:react_role]
  hash_arg.each do |k, v|
    message = "**__#{k.upcase}:__**\n\n"
    v.each { |name, data| message << ":#{name}::`#{data['name']}`\n\n" }
    sended = event.channel.send_message(message)
    collection.insert_one(message_id: sended.id, list: [])
    v.each do |_, data|
      sended.react(data['emoji'])
      collection.update_one({ message_id: sended.id},
                            '$set' => { list: collection.find(message_id: sended.id).first[:list] \
                              << { role_id: data['role_id'], emoji: data['emoji'], unset: data['toggle'] } })
    end
  end
  nil
end

@bot.command(:gg, required_permissions: [:ban_members], help_available: false) do |event|
  next unless ADMIN_ARRAY.include?(event.user.id)
  messages = event.message.channel.history(2)
  messages[1].author.add_role(475345487688630296)
  event.message.delete
end

@bot.command(:deleterole, required_permissions: [:ban_members], help_available: false) do |event|
  puts event.class
  roles = event.server.roles
  roles.each do |role|
    if role.name.include?('⭐')
      puts role.name.red
      role.delete
    else
      puts role.name.green
    end
    sleep(0.1)

  end
end

puts 'Admin CMD Loaded !'.green