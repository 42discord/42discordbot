
@bot.command(:user, max_args: 1, usage: 'user <xlogin>', description: 'Show a 42 user') do |event, login|
  log_chan = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['log']
  next event.respond 'Vous devez etre connécté pour effectuer cette action.' unless (channel = ft_linked?(event))

  if login && (parsed_user = @bot.parse_mention(login))
    next event.respond "Erreur, cet utilisateur n'existe pas." unless (db_user = User.find_by(discordid: parsed_user.id))

    login = db_user.login
  end

  begin
    user = login ? @ft_client.get("/v2/users/#{login}").parsed : @ft_client.get("/v2/users/#{User.find_by(discordid: event.user.id).login}").parsed
  rescue StandardError => _e
    event.respond "Erreur, cet utilisateur n'existe pas."
    next
  end
  coa = @ft_client.get("/v2/users/#{user['login']}/coalitions").parsed.first
  channel.send_embed do |embed|
    embed.title = user['usual_full_name'].to_s
    begin
      image = @bot.send_file(log_chan, Down.download(user['image_url'], open_timeout: 2))
      embed.image = Discordrb::Webhooks::EmbedImage.new(url: image.attachments.first.url)
    rescue Down::Error => e
      puts "Error while loading image : #{e.message}"
    end
    embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://www.defi-metiers.fr/sites/default/files/doc-kelios/Logo/2015/07/23/42_Final_sigle_seul_copie.png')
    embed.description = "Informations de #{user["login"]}"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: '42')
    embed.url = "http://profile.intra.42.fr/users/#{user["login"]}"
    embed.colour = (coa.nil? ? '#5c5c5c' : coa['color'].to_s)
    embed.add_field(name: 'Piscine', value: "#{user["pool_month"]} #{user["pool_year"]}") unless user['pool_month'].nil? && user['pool_year'].nil?
    embed.add_field(name: 'Campus', value: "#{user["campus"][0]['name']}") unless user["campus"][0]['name'].nil?
    embed.add_field(name: 'Staff', value: 'I am a Staff member !') if user['staff?'] == true
    user['location'].nil? ? embed.add_field(name: 'Location', value: 'Unavailable') : embed.add_field(name: 'Location', value: user['location'].to_s)
    embed.add_field(name: 'Wallet', value: user['wallet'].to_s)
    user['cursus_users'].each do |cursus|
      embed.add_field(name: cursus['cursus']['name'].to_s, value: "#{cursus["level"]} / #{cursus["grade"]}")
    end
  end
end

@bot.command(:whois, min_arg: 1, max_arg: 1, usage: 'whois @tag', description: 'Show the 42 login of member') do |event, login|
  log_chan = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['log']
  next event.respond 'Vous devez etre connécté pour effectuer cette action.' unless (channel = ft_linked?(event))

  if (parsed_user = @bot.parse_mention(login))
    next event.respond "Erreur, cet utilisateur n'existe pas." unless (db_user = User.find_by(discordid: parsed_user.id))

    login = db_user.login
    event.respond 'Le login est : ' + login
  else
    event.respond "Please tag a user."
  end
end

@bot.command(:make, required_permissions: [:ban_members]) do |event, login|
  if (parsed_user = @bot.parse_mention(login))
    next event.respond "Erreur, cet utilisateur n'existe pas." unless (db_user = User.find_by(discordid: parsed_user.id))

    login = db_user.login
    User.where('login': login).find_each(&:make_all)
  else
    event.respond "Please tag a user."
  end
end
puts 'Users CMD Loaded !'.green