# add_join_role : Used to save the roles to add when the server is joined. Use a mongo db to refer it
# The guard close is needed to deny every access to a non admin user

@bot.command(:add_join_role, min_args: 1, required_permissions: [:ban_members], help_available: false) do |event, *roles_id|
  next unless event.user.role?(YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['admin'])

  collection = @mongo_db[:add_join_role]
  roles_id.each { |v| collection.insert_one({ type: 'role_id', role_id: v }) }
  event.message.react('✅')
end

# delete_join_role : Same as 'add', but for the deleting way

@bot.command(:delete_join_role, min_args: 1, required_permissions: [:ban_members], help_available: false) do |event, *roles_id|
  next unless event.user.role?(YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['admin'])

  collection = @mongo_db[:add_join_role]
  roles_id.each { |v| collection.delete_one({ role_id: v }) }
  event.message.react('✅')
end

# list_join_role : Display the list of role to add when the server is joined

@bot.command(:list_join_role, required_permissions: [:ban_members], help_available: false) do |event|
  next unless event.user.role?(YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['admin'])

  collection = @mongo_db[:add_join_role]
  list_jrole = collection.find({ type: 'role_id' })
  next event.respond "nothing set" unless list_jrole.first

  list_jrole.each { |v| event << v[:role_id] }
  nil
end

puts 'JoinRole CMD Loaded !'.green