include DOTIW::Methods


@bot.command(:t3b, usage: 't3b', description: 'Show the next t3b at Honore de Balzac', aliases: [:t3]) do |event|
  asniere = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100112003%3AT3B/stop_areas/stop_area%3A0%3ASA%3A59932/routes/route%3A0%3A1001120030002A/departures?count=2", @navitia)
  vincene = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100112003%3AT3B/routes/route%3A0%3A1001120030002R/stop_areas/stop_area%3A0%3ASA%3A59932/departures?count=2", @navitia)

  event.channel.send_embed do |embed|
    embed.color =  '#' + asniere['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/20/36/4yqf.png')
    embed.description = "#{asniere['departures'][0]['display_informations']['label']} - #{asniere['departures'][0]['stop_point']['name']}"
    embed.add_field(name: asniere['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: asniere['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: vincene['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, vincene['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: vincene['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, vincene['departures'][1]['stop_date_time']['departure_date_time']))
  end
end

@bot.command(:rerc, usage: 'rerc', description: 'Show the next RER C at Porte de Clichy', aliases: [:rer]) do |event|
  rer = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A800%3AC/stop_areas/stop_area%3A0%3ASA%3A8711127/departures?count=25", @navitia)
  event.channel.send_embed do |embed|
    embed.color =  '#' + rer['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/20/36/yfqv.png')
    embed.description = "#{rer['departures'][0]['display_informations']['label']} - #{rer['departures'][0]['stop_point']['name']}"
    rer['departures'].each do |dep|
      embed.add_field(name: dep['display_informations']['direction'], value: distance_of_time_in_words(Time.now, dep['stop_date_time']['departure_date_time']))
    end
  end

end

@bot.command(:m13, usage: 'm13', description: 'Show the next Metro 13 at Porte de Clichy and Porte de Saint Ouen') do |event|
  asniere_c = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100110013%3A13/stop_areas/stop_area%3A0%3ASA%3A8711127/routes/route%3A0%3A1001100130001A/departures?count=2&" ,@navitia)
  montrouge_c = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100110013%3A13/stop_areas/stop_area%3A0%3ASA%3A8711127/routes/route%3A0%3A1001100130001R/departures?count=2&", @navitia)
  asniere_o = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100110013%3A13/stop_areas/stop_area%3A0%3ASA%3A59526/routes/route%3A0%3A1001100130001A/departures?count=2&", @navitia)
  montrouge_o = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100110013%3A13/stop_areas/stop_area%3A0%3ASA%3A59526/routes/route%3A0%3A1001100130001R/departures?count=2&", @navitia)

  event.channel.send_embed do |embed|
    embed.color =  '#' + asniere_c['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://cdn.discordapp.com/emojis/600618768934371362.png')
    embed.description = "#{asniere_c['departures'][0]['display_informations']['label']} - #{asniere_c['departures'][0]['stop_point']['name']}"
    embed.add_field(name: asniere_c['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere_c['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: asniere_c['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere_c['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: montrouge_c['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, montrouge_c['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: montrouge_c['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, montrouge_c['departures'][1]['stop_date_time']['departure_date_time']))
  end

  event.channel.send_embed do |embed|
    embed.color =  '#' + asniere_o['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://cdn.discordapp.com/emojis/600618768934371362.png')
    embed.description = "#{asniere_o['departures'][0]['display_informations']['label']} - #{asniere_o['departures'][0]['stop_point']['name']}"
    embed.add_field(name: asniere_o['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere_o['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: asniere_o['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, asniere_o['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: montrouge_o['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, montrouge_o['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: montrouge_o['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, montrouge_o['departures'][1]['stop_date_time']['departure_date_time']))
  end
end

@bot.command(:n15, usage: 'n15', description: 'Show the next Noctilien 15 at Porte de Clichy') do |event|
  noc_v = get_json("https://api.navitia.io/v1/coverage/fr-idf/routes/route%3A0%3A1009877820001A/stop_points/stop_point%3A0%3ASP%3A59%3A4036731/departures?count=2&", @navitia)
  noc_g = get_json("https://api.navitia.io/v1/coverage/fr-idf/routes/route%3A0%3A1009877820001R/stop_points/stop_point%3A0%3ASP%3A59%3A4472777/departures?count=2&", @navitia)



  event.channel.send_embed do |embed|
    embed.color =  '#' + noc_v['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/20/36/eu6b.png')
    embed.description = "#{noc_v['departures'][0]['display_informations']['label']} - #{noc_v['departures'][0]['stop_point']['name']}"
    embed.add_field(name: noc_v['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_v['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_v['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_v['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_g['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_g['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_g['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_g['departures'][1]['stop_date_time']['departure_date_time']))
  end
end

@bot.command(:n51, usage: 'n51', description: 'Show the next Noctilien 51 at Porte de Clichy') do |event|
  noc_l = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100987753%3AN51/routes/route%3A0%3A1009877530003A/stop_points/stop_point%3A0%3ASP%3A59%3A4036731/departures?count=2&", @navitia)
  noc_e = get_json("https://api.navitia.io/v1/coverage/fr-idf/lines/line%3A0%3A100987753%3AN51/routes/route%3A0%3A1009877530003R/stop_points/stop_point%3A0%3ASP%3A59%3A4036733/departures?count=2&", @navitia)



  event.channel.send_embed do |embed|
    embed.color =  '#' + noc_l['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/20/36/dr2o.png')
    embed.description = "#{noc_l['departures'][0]['display_informations']['label']} - #{noc_l['departures'][0]['stop_point']['name']}"
    embed.add_field(name: noc_l['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_l['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_l['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_l['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_e['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_e['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: noc_e['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, noc_e['departures'][1]['stop_date_time']['departure_date_time']))
  end
end

@bot.command(:noctilien, usage: 'n15', description: 'Show the next Noctilien at Porte de Clichy', aliases: [:noct]) do |event|
  @bot.commands[:n15].call(event, [''])
  @bot.commands[:n51].call(event, [''])
end


@bot.command(:bus, usage: 'bus', description: 'Show the next Bus at Porte de Clichy') do |event|
  bus = get_json("https://api.navitia.io/v1/coverage/fr-idf/stop_areas/stop_area%3A0%3ASA%3A8711127/commercial_modes/commercial_mode%3ABus/departures?count=25&", @navitia)

  event.channel.send_embed do |embed|
    embed.color =  '#000000'
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/20/36/tcu2.png')
    embed.description = "#{bus['departures'][0]['display_informations']['commercial_mode']} - #{bus['departures'][0]['stop_point']['name']}"
    bus['departures'].each do |b|
      embed.add_field(name: b['display_informations']['label'] + ' > ' + b['display_informations']['direction'], value: distance_of_time_in_words(Time.now, b['stop_date_time']['departure_date_time']))
    end
  end
end


@bot.command(:m14, usage: 'm14', description: 'Show the next Metro 14 at Porte de de Clichy') do |event|

  olympiade = get_json("https://api.navitia.io/v1/coverage/fr-idf/stop_points/stop_point%3A0%3ASP%3A59943/lines/line%3A0%3A100110014%3A14/routes/route%3A0%3A1001100140001A/departures?count=2&", @navitia)
  stouen = get_json("https://api.navitia.io/v1/coverage/fr-idf/stop_points/stop_point%3A0%3ASP%3A59943/lines/line%3A0%3A100110014%3A14/routes/route%3A0%3A1001100140001R/departures?count=2&", @navitia)

  event.channel.send_embed do |embed| 
    embed.color =  olympiade['departures'][0]['display_informations']['color'].to_s
    embed.thumbnail =  Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://zupimages.net/up/21/05/gdzm.png')
    embed.description = "#{olympiade['departures'][0]['display_informations']['label']} - #{olympiade['departures'][0]['stop_point']['name']}"
    embed.add_field(name: olympiade['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, olympiade['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: olympiade['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, olympiade['departures'][1]['stop_date_time']['departure_date_time']))
    embed.add_field(name: stouen['departures'][0]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, stouen['departures'][0]['stop_date_time']['departure_date_time']))
    embed.add_field(name: stouen['departures'][1]['display_informations']['direction'], value: distance_of_time_in_words(Time.now, stouen['departures'][1]['stop_date_time']['departure_date_time']))

  end

end

puts 'Ratp CMD Loaded !'.green
