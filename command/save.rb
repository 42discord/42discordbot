@bot.command(:save, aliases: %i[register login]) { 'Pour vous enregister, vous pouvez vous rendre sur ce lien : https://42discord.student42.fr/' }

@bot.command(:un_save, aliases: %i[unsave unlink un_link]) do |event|
  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
  next unless (user = User.find_by(discordid: event.user.id))

  event.user.remove_role(hash_arg[:server]['student_role']) if event.user.role?(hash_arg[:server]['student_role'])
  curious_role = event.server.role(hash_arg[:server]['curious_role'])
  event.user.add_role(curious_role)
  event.user.roles&.each { |r| event.user.remove_role(r.id) if r.position < curious_role.position }
  user.destroy
  'You have been correctly deleted from the database'
end