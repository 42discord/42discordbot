# frozen_string_literal: true

# Patching Imgur API error when for some reason, no date exist in the data
class Imgur::Image

  HTML_PATH = 'https://imgur.com/'

  def initialize(data)
    @id = data['id']
    @title = data['title']
    @description = data['description']
    @date = Time.now
    @type = data['type']
    @animated = data['animated']
    @width = data['width']
    @height = data['height']
    @size = data['size']
    @views = data['views']
    @bandwidth = data['bandwidth']
    @favorite = data['favorite']
    @nsfw = data['nsfw']
    @section = data['section']
    @deletehash = data['deletehash']
    @link = data['link']
    @html_link = HTML_PATH + @id
  end
end