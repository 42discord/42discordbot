require 'rubygems'
require 'bundler/setup'
require 'discordrb/webhooks'

WEBHOOK_URL = ENV["DISCORD_WEBHOOK"].freeze
puts ARGV[0]
client = Discordrb::Webhooks::Client.new(url: WEBHOOK_URL)
client.execute do |builder|
  builder.add_embed do |embed|
    embed.url = ENV["CI_PIPELINE_URL"]
    embed.title = ENV["CI_COMMIT_MESSAGE"]
    embed.description = ENV["GITLAB_USER_LOGIN"]
    embed.colour = ARGV[0] == 'success' ? "#24b54b" : "#ff0048"
    embed.timestamp = Time.now
    embed.add_field(name: 'Metrics', value: File.read('metrics.txt'))
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: ENV["CI_COMMIT_BRANCH"])
  end
end